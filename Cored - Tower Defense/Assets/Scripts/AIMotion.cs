﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMotion : MonoBehaviour {
    public float speed = 0.1f;
    public Vector2 direction;
    public GameObject enemy;
    public GameObject spawn;
    AudioSource squish;
    //-------------------------
    private float actualSpeed;
    private SceneManager sceneManagerScript;
    private Vector3 deltaMovement;
    public float rotate = 1.0f;
    bool isThisInvisible = false;
    private int coinx = 0;
    private int coiny = 0;
    private float ex = 1f;
    private float why = 1f;
    Renderer r;
    // Use this for initialization
    void Start () {
        squish = GetComponent<AudioSource>();
        coinx = Random.Range(1, 10);
        if (coinx > 5)
        {
            ex *= -1;
        }
        coiny = Random.Range(1, 10);
        if (coiny > 5)
        {
            why *= -1;
        }
        

        r = GetComponent<Renderer>();
        direction = new Vector2(ex,why);
        actualSpeed = speed;
    }

    public void setDirection(Vector2 dir)
    {
        direction = dir;
    }

    private Vector2 IsThisOffScreen()
    {
        if (r.isVisible)
        {
            isThisInvisible = false;
            return new Vector2(1, 1);
        }
        if (isThisInvisible)
        {
            return new Vector2(1, 1);
        }
        float xFix = 1;
        float yFix = 1;
        Vector3 viewPortPosition = Camera.main.WorldToViewportPoint(transform.position);
        //Debug.Log(viewPortPosition.ToString());
        if (viewPortPosition.x > 1 || viewPortPosition.x < 0)
            xFix = -1;
        if (viewPortPosition.y > 1 || viewPortPosition.y < 0)
            yFix = -1;
        isThisInvisible = true;
        return new Vector2(xFix, yFix);

    }
    // Update is called once per frame
    void Update () {

        transform.Rotate(0, 0, rotate);
        Vector3 oldPosition = transform.position;
        Vector2 newPosition = actualSpeed * direction * Time.deltaTime ;
        Vector2 temp = IsThisOffScreen();
        deltaMovement = new Vector3(newPosition.x, newPosition.y, oldPosition.z);
        transform.position += deltaMovement;
        transform.position *= temp;


        #region position update
        //Debug.Log("Direction " + direction.ToString());
        //Vector3 position = transform.position;
        //Vector3 newPosition = actualSpeed * new Vector3(direction.x, direction.y, position.z);
        //position += newPosition;
        //transform.position = position;
        #endregion
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject Scoring = GameObject.Find("SceneManager");
        SceneManager scoring = Scoring.GetComponent<SceneManager>();
        GameObject go = collision.gameObject;
        if (go.tag == "PlayerBullet")
        {
            for (int i = 0; i < 4; i++)
            {
                GameObject temp = GameObject.Instantiate(enemy);
                temp.transform.position = spawn.transform.position;
                temp.transform.rotation = spawn.transform.rotation;
                //temp.transform.position = Vector3.;

            }
            squish.Play();
            //scoring.Score += 1;
            Destroy(this.gameObject);
            //direction.y *= -1;
            //sceneManagerScript.incBounceCount();

        }
        //Destroy(this.gameObject);

        if (go.tag == "Asteroid")
        {
            //Destroy(this.gameObject);
            direction.y *= -1;
            direction.x *= -1;
            //sceneManagerScript.incBounceCount();

        }
    }

}
