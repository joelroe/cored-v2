﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public GameObject particle;
    public GameObject chunk1;
    public GameObject chunk2;
    public GameObject chunk3;
    public Sprite normal; // -----
    public Sprite slight;//  stuff for sprite damage
    private SpriteRenderer spriteRenderer; //-----
    public Sprite critical;// ---
    public int maxhp;//---
    public AudioSource pop;
    private float percent;
    public GameObject explosion;
    public GameObject[] waypoints;
    public float enemySpeed = 5;
    public int health;
    int currentWaypoint = 0;

    public Vector2 dir;
    private GameObject currentWaypointTarget = null;
    private SceneManager sceneManagerScript;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>(); // we are accessing the SpriteRenderer that is attached to the Gameobject
        if (spriteRenderer.sprite == null) // if the sprite on spriteRenderer is null then
            spriteRenderer.sprite = normal; // set the sprite to sprite1

        GameObject sceneManager = GameObject.FindGameObjectWithTag("SceneManager");
        sceneManagerScript = sceneManager.GetComponent<SceneManager>();
        int level = sceneManagerScript.getLevel();
        if (level > 10)
        {
            for (int i = 10; i < level; i++)
            {
                maxhp = maxhp * 12 / 10;
            }
            health = maxhp;
        }
        fetchWaypoints();
    }

    public int findClosestIndex()
    {
        int index = 0;
        float distance = 100;
        for (int i = 0; i < waypoints.Length; i++)
        {
            Vector3 temp = waypoints[i].transform.position - this.transform.position;
            Debug.Log("Index - " + index + " - " + temp.magnitude);
            if (temp.magnitude < distance)
            {
                index = i;
                distance = temp.magnitude;
            }
        }

        Debug.Log("Closest Index - " + index);

        return index;
    }

    public Vector3 evaluateWaypoint()
    {
        dir = waypoints[currentWaypoint].transform.position -
            this.transform.position;
        if (dir.magnitude < 0.1f)
        {
            currentWaypoint++;
            if (currentWaypoint >= waypoints.Length)
            {
                sceneManagerScript.damageCore(maxhp);
                Destroy(this.gameObject);
            }
        }

        dir.Normalize();

        return dir;
    }

    public void Hit(int damage)
    {
        health -= damage;
        percent = health / maxhp;
        if (health < 0)
        {
            Die();
        }
        else if (percent < .40)
        {
            spriteRenderer.sprite = critical;
        }
        else if (percent < .75)
        {
            spriteRenderer.sprite = slight;
        }
        GameObject temp;
        for (int i = 0; i < damage / 10; i++)
        {
            temp = GameObject.Instantiate(particle);
            temp.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 0f);
            Destroy(temp, .5f);
        }
    }

    private void Die()
    {
        //pop.Play();
        sceneManagerScript.setMoney(sceneManagerScript.getMoney() + 25);
        GameObject temp;
        GameObject temp2;
        GameObject temp3;
        GameObject temp4;

        //for (int i = 0; i < 10; i++)
        //{
        //    temp = GameObject.Instantiate(particle);
        //    temp.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 0f);
        //    Destroy(temp, 1f);
        //} 

        

        temp = GameObject.Instantiate(chunk1);
        temp.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 0f);
        Destroy(temp, 2f);

        temp2 = GameObject.Instantiate(chunk2);
        temp2.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 0f);
        Destroy(temp2, 2f);

        temp3 = GameObject.Instantiate(chunk3);
        temp3.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 0f);
        Destroy(temp3, 2f);

        temp4 = GameObject.Instantiate(explosion);
        temp4.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 0f);
        Destroy(temp4, 2f);

        

        Destroy(this.gameObject);
    }

    private void fetchWaypoints()
    {
        int temp = sceneManagerScript.howManyPoints();
        waypoints = new GameObject[temp];
        for (int i = 0; i < temp; i++)
        {
            waypoints[i] = sceneManagerScript.fetchWaypoint(i);
        }
    }

    private void findClosest()
    {
        float shortestDistance = float.MaxValue;
        Vector3 shortestDirection = Vector2.zero;
        Vector3 dirTemp = Vector3.zero;
        foreach (GameObject g in waypoints)
        {
            dirTemp = g.transform.position - this.transform.position;
            dirTemp.y = 0;
            float distance = dirTemp.magnitude;
            if (distance <= 0.6)
                continue;
            if (distance < shortestDistance)
            {
                dirTemp.y = 0;
                shortestDirection = dirTemp;
                shortestDistance = distance;
                currentWaypointTarget = g;
            }
        }

        dir = shortestDirection;
    }
    public Vector2 findClosestWaypoint()
    {

        Vector3 foo = this.transform.position -
            currentWaypointTarget.transform.position;
        float distanceToTarget = foo.magnitude;

        if (distanceToTarget < 0.6)
        {
            findClosest();
        }

        return dir.normalized;
    }
    // Update is called once per frame
    void Update()
    {
        Vector3 dir = Vector3.zero;
        float localSpeed = 0;
        dir = evaluateWaypoint();
        //Debug.Log(dir);
        if(sceneManagerScript.hasDieSequenceStarted())
        {
            localSpeed = 0f;
        }
        else
        {
            localSpeed = enemySpeed;
        }
        //Debug.Log(localSpeed);
        this.transform.position += dir * localSpeed * Time.deltaTime;
    }
}
