﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleSpawner : MonoBehaviour
{
    public GameObject SingleTurret, SingleTemplate;
    public GameObject MultiTurret, MultiTemplate;
    public GameObject SniperTurret, SniperTemplate;
    public GameObject GatlingTurret, GatlingTemplate;
    public GameObject Cannon, CannonTemplate;

    private GameObject temp;
    private bool press = false;
    private int turretpick = 1;
    private SceneManager sceneManagerScript;

    // Start is called before the first frame update
    void Start()
    {
        GameObject sceneManager = GameObject.FindGameObjectWithTag("SceneManager");
        sceneManagerScript = sceneManager.GetComponent<SceneManager>();
    }

    void Awake()
    {
        //Messenger.AddListener("start game", StartGame);
    }

    // Update is called once per frame
    void Update()
    {
        if (press == true)
        {
            temp.transform.position = getMouseVector();
        }
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (press == true)
            {
                if(canSpawn())
                {
                    switch (turretpick)
                    {
                        case 1:
                            singlespawn();
                            break;
                        case 2:
                            multispawn();
                            break;
                        case 3:
                            sniperspawn();
                            break;
                        case 4:
                            gatlingspawn();
                            break;
                        case 5:
                            cannonspawn();
                            break;
                        default:
                            press = false;
                            break;
                    }
                    Destroy(temp.gameObject);
                }
                else
                {
                    press = false;
                    Destroy(temp.gameObject);
                    sceneManagerScript.notify("You cant stack turrets!", 1.5f);
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            press = false;
            Destroy(temp.gameObject);
        }
    }

    public void singlepress()
    {
        if (sceneManagerScript.getMoney() >= 100)
        {
        turretpick = 1;
        press = true;
        setDragger(SingleTemplate);
        }
        else
        {
            sceneManagerScript.notify("insufficient funds\n$100", 1.5f);
        }
    }

    public void singlespawn()
    {
        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono);

        // make z position the Z position of the pefab object
        Vector3 adjustZ = new Vector3(worldPoint.x, worldPoint.y, SingleTurret.transform.position.z);

        Spawn(adjustZ, SingleTurret);

        sceneManagerScript.setMoney(sceneManagerScript.getMoney() - 100);

        press = false;
    }

    public void multipress()
    {
        if (sceneManagerScript.getMoney() >= 500)
        {
            turretpick = 2;
            press = true;
            setDragger(MultiTemplate);
        }
        else
        {
            sceneManagerScript.notify("insufficient funds\n$500", 1.5f);
        }
    }

    public void multispawn()
    {
        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono);

        // make z position the Z position of the pefab object
        Vector3 adjustZ = new Vector3(worldPoint.x, worldPoint.y, MultiTurret.transform.position.z);

        Spawn(adjustZ, MultiTurret);

        sceneManagerScript.setMoney(sceneManagerScript.getMoney() - 500);


        press = false;
    }

    public void sniperpress()
    {
        if (sceneManagerScript.getMoney() >= 1000)
        {
            turretpick = 3;
            press = true;
            setDragger(SniperTemplate);
        }
        else
        {
            sceneManagerScript.notify("insufficient funds\n$1000", 1.5f);
        }
    }

    public void sniperspawn()
    {
        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono);

        // make z position the Z position of the pefab object
        Vector3 adjustZ = new Vector3(worldPoint.x, worldPoint.y, SniperTurret.transform.position.z);

        Spawn(adjustZ, SniperTurret);

        sceneManagerScript.setMoney(sceneManagerScript.getMoney() - 1000);

        press = false;
    }

    public void gatlingpress()
    {
        if (sceneManagerScript.getMoney() >= 2500)
        {
            turretpick = 4;
            press = true;
            setDragger(GatlingTemplate);
        }
        else
        {
            sceneManagerScript.notify("insufficient funds\n$2500", 1.5f);
        }
    }

    public void gatlingspawn()
    {
        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono);

        // make z position the Z position of the pefab object
        Vector3 adjustZ = new Vector3(worldPoint.x, worldPoint.y, GatlingTurret.transform.position.z);

        Spawn(adjustZ, GatlingTurret);

        sceneManagerScript.setMoney(sceneManagerScript.getMoney() - 2500);

        press = false;
    }

    public void cannonpress()
    {
        turretpick = 5;
        press = true;
        setDragger(CannonTemplate);
    }

    public void cannonspawn()
    {
        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono);

        // make z position the Z position of the pefab object
        Vector3 adjustZ = new Vector3(worldPoint.x, worldPoint.y, Cannon.transform.position.z);

        Spawn(adjustZ, Cannon);

        press = false;
    }

    public void Spawn(Vector3 position, GameObject spawnee)
    {
        Instantiate(spawnee).transform.position = position;
    }

    public void setDragger(GameObject spawnee)
    {
        temp = Instantiate(spawnee);
        temp.transform.position = getMouseVector();
    }

    private Vector3 getMouseVector()
    {
        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono);

        // make z position the Z position of the pefab object
        Vector3 adjustZ = new Vector3(worldPoint.x, worldPoint.y, SingleTurret.transform.position.z);

        return adjustZ;
    }

    public void menubutton()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
    }
    private bool canSpawn()
    {
        return temp.GetComponent<TemplateChecker>().getCanPlace();
    }

    //private void OnTriggerEnter2D(Collider2D collision)
    //{
        
    //    if (go.tag == "Unstackable")
    //    {
    //        press = false;
    //        Destroy(temp.gameObject);
    //        sceneManagerScript.notify("Collision Detected", 1.5f);
    //    }
    //}

}
