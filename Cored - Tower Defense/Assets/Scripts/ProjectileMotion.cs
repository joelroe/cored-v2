﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMotion : MonoBehaviour
{
    public float speed = 1f;
    public Vector2 direction;

    private float actualSpeed;


    // Use this for initialization
    void Start()
    {
        direction = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        actualSpeed = speed;
    }

    public void setDirection(Vector2 dir)
    {
        direction = dir;
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 oldPosition = transform.position;
        Vector2 newPosition = actualSpeed * direction * Time.deltaTime;
        Vector3 temp = new Vector3(newPosition.x, newPosition.y, oldPosition.z);
        transform.position += temp;
    }

}
