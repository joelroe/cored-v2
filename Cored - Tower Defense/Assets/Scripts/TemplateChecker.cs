﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemplateChecker : MonoBehaviour
{

    private bool canPlace = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool getCanPlace()
    {
        return canPlace;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject go = collision.gameObject;

        if (go.tag == "Unstackable")
        {
            canPlace = false;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        canPlace = true;
    }
}
