﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiSpawner : MonoBehaviour
{
    public GameObject turret, temp;
    private bool press = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (press = true)
        {
            temp.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono);
        }
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (press == true)
            {
                Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono);

                // make z position the Z position of the pefab object
                Vector3 adjustZ = new Vector3(worldPoint.x, worldPoint.y, turret.transform.position.z);

                Spawn(adjustZ);

                press = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            press = false;
            Destroy(temp.gameObject);
        }
    }

    public void buttonpress()
    {
        press = true;
        temp = Instantiate(turret);
    }

    public void Spawn(Vector3 position)
    {
        Instantiate(turret).transform.position = position;
    }
}
