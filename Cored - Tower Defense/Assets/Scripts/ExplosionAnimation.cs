﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionAnimation : MonoBehaviour
{
    Color color;
    //public AudioSource explosion;
    // Start is called before the first frame update
    void Start()
    {
        //explosion.Play();
        color = this.GetComponent<SpriteRenderer>().color;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(color.a);
        if (color.a > 0)
        {
            color.a -= .05f;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
