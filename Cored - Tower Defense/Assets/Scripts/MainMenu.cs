﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void quitgame()
    {
        Application.Quit();
    }
    public void level1()
    {
        PlayerPrefs.SetInt("Difficulty", 0);
        UnityEngine.SceneManagement.SceneManager.LoadScene("SampleScene");
    }
    public void level2()
    {
        PlayerPrefs.SetInt("Difficulty", 1);
        UnityEngine.SceneManagement.SceneManager.LoadScene("SampleScene");
    }
    public void levelselect()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Level Select");
        }
    
    public void Menu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
    }
}
