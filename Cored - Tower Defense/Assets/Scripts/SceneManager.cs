﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneManager : MonoBehaviour
{
    public GameObject[] waypoints;
    public LineRenderer[] Lines;
    public GameObject Enemy1, Enemy2, Enemy3, Core, Explosion, frag1, frag2, frag3, fissure, turretexplosion;
    public float spawnDelay = 2.0f;
    public Text notifyText, moneyText, levelUpText;

    public Sprite normal; // -----
    public Sprite slight;//  stuff for sprite damage
    private SpriteRenderer spriteRenderer; //-----
    public Sprite critical;// ---

    private bool canSpawn = true, DieSequenceStarted = false, LevelupSequence = false;
    private int level = 1, enemies = 10, money = 500, CoreHealth = 500, emission = 0, difficulty = 0;

    // Start is called before the first frame update
    void Start()
    {
        difficulty = PlayerPrefs.GetInt("Difficulty", 0);
        if (difficulty == 1)
        {
            setDifficultWaypoints();
        }
        for (int i = 0; i < 11; i++)
        {
            Lines[i].SetPosition(0, waypoints[i].transform.position);
            Lines[i].SetPosition(1, waypoints[i+1].transform.position);
            Lines[i].enabled = true;
            //AddColliderToLine(Lines[i], waypoints[i].transform.position, waypoints[i + 1].transform.position);
        }

        GameObject temp = GameObject.Instantiate(Core);
        temp.transform.position = new Vector3(waypoints[11].transform.position.x, waypoints[11].transform.position.y,-1f);

        spriteRenderer = GameObject.FindGameObjectWithTag("Core").GetComponent<SpriteRenderer>(); // we are accessing the SpriteRenderer that is attached to the Gameobject
        if (spriteRenderer.sprite == null) // if the sprite on spriteRenderer is null then
            spriteRenderer.sprite = normal; // set the sprite to sprite1

        updateText();
        canSpawn = false;
        StartCoroutine(canSpawnTimer(5f));
    }

    // Update is called once per frame
    void Update()
    {
        if (canSpawn && !DieSequenceStarted)
        {
            Spawn();
        }

        if (!canSpawn && !DieSequenceStarted && !LevelupSequence && enemies == 0)
        {
            checkNextLevel();
        }

        if(!DieSequenceStarted)
        {
            manageCore();
        }

        if (LevelupSequence)
        {
            levelUpText.transform.position = new Vector3(500f, levelUpText.transform.position.y + 5f, 0f);
        }
    }

    public int howManyPoints()
    {
        return waypoints.Length;
    }

    public GameObject fetchWaypoint(int i)
    {
        return waypoints[i];
    }

    public int getMoney()
    {
        return money;
    }

    public void setMoney(int newAmount)
    {
        money = newAmount;
        updateText();
    }

    public void notify(string message, float duration)
    {
        notifyText.text = message;
        StartCoroutine(clearNotification(duration));
    }

    public void damageCore(int damage)
    {
        CoreHealth -= damage;
    }

    public bool hasDieSequenceStarted()
    {
        return DieSequenceStarted;
    }

    public int getLevel()
    {
        return level;
    }

    IEnumerator clearNotification(float duration)
    {
        yield return new WaitForSeconds(duration);
        notifyText.text = "";
    }

    IEnumerator Die()
    {
        Debug.Log("Die Called");
        float waitTime = 2f;
        GameObject coreObject = GameObject.FindGameObjectWithTag("Core"), temp;
        GameObject[] existingTurrets = GameObject.FindGameObjectsWithTag("Unstackable");

        temp = GameObject.Instantiate(Explosion);
        temp.transform.position = coreObject.transform.position;
         
        Destroy(coreObject);
        

        for (int i = 0; i < existingTurrets.Length; i++)
        {
            yield return new WaitForSeconds(waitTime);
            temp = GameObject.Instantiate(turretexplosion);
            temp.transform.position = existingTurrets[i].transform.position;
            Destroy(existingTurrets[i].gameObject);
            waitTime /= 2f;
        }

        yield return new WaitForSeconds(3f);
        UnityEngine.SceneManagement.SceneManager.LoadScene("End");
    }

        private void updateText()
    {
        moneyText.text = "$" + money.ToString();
    }

    private void manageCore()
    {
        

        if (CoreHealth < 0)
        {
            DieSequenceStarted = true;
            StartCoroutine(Die());
        }
        else if (CoreHealth < 100)
        {
            spriteRenderer.sprite = critical;
            if (emission == 0)
                {
                    GameObject coreObject = GameObject.FindGameObjectWithTag("Core"), temp;
                    temp = GameObject.Instantiate(fissure);
                    temp.transform.position = coreObject.transform.position;
                    emission = 1;
                }

        }
        else if (CoreHealth < 250)
        {
            spriteRenderer.sprite = slight;
        }
        else
        {
            spriteRenderer.sprite = normal;
        }
    }

    private void Spawn()
    {
        float rand = Random.Range(0f, 1f);
        GameObject temp;
        if(level < 5)
        {
            temp = GameObject.Instantiate(Enemy1);
        }
        else if (level < 10)
        {
            if (rand < .75f)
            {
                temp = GameObject.Instantiate(Enemy1);
            }
            else
            {
                temp = GameObject.Instantiate(Enemy3);
            }
        }
        else
        {
            if (rand < .7f)
            {
                temp = GameObject.Instantiate(Enemy1);
            }
            else if (rand < .9f)
            {
                temp = GameObject.Instantiate(Enemy2);
            }
            else
            {
                temp = GameObject.Instantiate(Enemy3);
            }
        }

        temp.transform.position = new Vector3(waypoints[0].transform.position.x, waypoints[0].transform.position.y, -1f);

        checkLevel();
        if(canSpawn == true)
        {
            canSpawn = false;
            StartCoroutine(canSpawnTimer(spawnDelay));
        }
    }

    IEnumerator canSpawnTimer(float wait)
    {
        yield return new WaitForSeconds(wait);
        canSpawn = true;
    }

    private void checkLevel()
    {
        enemies--;
        if (enemies <= 0)
        {
            canSpawn = false;
        }
        //Debug.Log("Level: " + level + ", Enemies: " + enemies + ", spawnDelay: " + spawnDelay);
    }

    private void checkNextLevel()
    {
        if(GameObject.FindWithTag("Enemy") == null)
            StartCoroutine(levelUpWaitTimer());
    }

    IEnumerator levelUpWaitTimer()
    {
        levelUpText.text = "Level " + (level + 1).ToString();
        LevelupSequence = true;
        yield return new WaitForSeconds(10f);
        LevelupSequence = false;
        levelUpText.transform.position = new Vector3(0f, -500f, 0f);
        level++;
        enemies = 10 * level;
        spawnDelay = 2.0f / level;
        canSpawn = true;
    }

    private void setDifficultWaypoints()
    {
        waypoints[0].transform.position = new Vector3(-10f, 0f, -.9f);
        waypoints[1].transform.position = new Vector3(-5f, 0f, -.9f);
        waypoints[2].transform.position = new Vector3(0f, 5f, -.9f);
        waypoints[3].transform.position = new Vector3(0f, 4f, -.9f);
        waypoints[4].transform.position = new Vector3(0f, 3f, -.9f);
        waypoints[5].transform.position = new Vector3(0f, 2f, -.9f);
        waypoints[6].transform.position = new Vector3(0f, -2f, -.9f);
        waypoints[7].transform.position = new Vector3(0f, -3f, -.9f);
        waypoints[8].transform.position = new Vector3(0f, -4f, -.9f);
        waypoints[9].transform.position = new Vector3(0f, -5f, -.9f);
        waypoints[10].transform.position = new Vector3(5f, 0f, -.9f);
        waypoints[11].transform.position = new Vector3(8f, 0f, -.9f);
    }

    //private void AddColliderToLine(LineRenderer line, Vector3 startPoint, Vector3 endPoint)
    //{
    //    //create the collider for the line
    //    BoxCollider lineCollider = new GameObject("LineCollider").AddComponent<BoxCollider>();
    //    //set the collider as a child of your line
    //    lineCollider.transform.parent = line.transform;
    //    // get width of collider from line 
    //    float lineWidth = line.endWidth;
    //    // get the length of the line using the Distance method
    //    float lineLength = Vector3.Distance(startPoint, endPoint);
    //    // size of collider is set where X is length of line, Y is width of line
    //    //z will be how far the collider reaches to the sky
    //    lineCollider.size = new Vector3(lineLength, lineWidth, 1f);
    //    // get the midPoint
    //    Vector3 midPoint = (startPoint + endPoint) / 2;
    //    // move the created collider to the midPoint
    //    lineCollider.transform.position = midPoint;


    //    //heres the beef of the function, Mathf.Atan2 wants the slope, be careful however because it wants it in a weird form
    //    //it will divide for you so just plug in your (y2-y1),(x2,x1)
    //    float angle = Mathf.Atan2((endPoint.z - startPoint.z), (endPoint.x - startPoint.x));

    //    // angle now holds our answer but it's in radians, we want degrees
    //    // Mathf.Rad2Deg is just a constant equal to 57.2958 that we multiply by to change radians to degrees
    //    angle *= Mathf.Rad2Deg;

    //    //were interested in the inverse so multiply by -1
    //    angle *= -1;
    //    // now apply the rotation to the collider's transform, carful where you put the angle variable
    //    // in 3d space you don't wan't to rotate on your y axis
    //    lineCollider.transform.Rotate(0, angle, 0);
    //}
}
